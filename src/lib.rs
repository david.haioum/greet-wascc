extern crate wascc_actor as actor;

use std::collections::HashMap;

use actor::prelude::*;
use log::{info};

actor_handlers! { codec::http::OP_HANDLE_REQUEST => greet,
codec::core::OP_HEALTH_REQUEST => health }

pub fn greet(r: codec::http::Request) -> HandlerResult<codec::http::Response> {
    let mut hm = HashMap::new();
    let mut code = 200;
    hm.insert("accept".to_string(), "application/json".to_string());

    let path = String::from(&r.path).trim_start_matches('/').to_string();

    match path.trim() {
        "hello" => {
            hm.insert("Status".to_string(), "it's ok".to_string());
            code = 200;
        },
        "redirect" => {
            hm.insert("Location".to_string(), "/hello".to_string());
            code = 301;
        },
        _ => {
            hm.insert("dummy".to_string(), "value".to_string());
            code = 200;
        }
    };

    let res = codec::http::Response {
        status_code: code,
        status: "OK".to_string(),
        header: hm,
        body: b"Hello, World".to_vec(),
    };

    //println!(&format!("Received HTTP request: {:?}", &r));
    info!("res: {:?}", res);
    Ok(res)
}

pub fn health(_h: codec::core::HealthRequest) -> HandlerResult<()> {
    Ok(())
}
