CARGO   := "cargo"
TARGET  := "target/wasm32-unknown-unknown"
DEBUG   := TARGET + "/debug"
RELEASE := TARGET + "/release"
KEYDIR  := ".keys"
VERSION := "0.5"
NAME    := "greet"

all: build

build: keys
	@{{CARGO}} build
	wascap sign {{DEBUG}}/{{NAME}}.wasm {{DEBUG}}/{{NAME}}_signed.wasm -i {{KEYDIR}}/account.nk -u {{KEYDIR}}/module.nk -s -l -n {{NAME}}

release: keys
	@{{CARGO}} build --release
	wascap sign {{RELEASE}}/{{NAME}}.wasm {{RELEASE}}/{{NAME}}_signed.wasm -i {{KEYDIR}}/account.nk -u {{KEYDIR}}/module.nk -s -l -n {{NAME}}

push:
	wasm-to-oci push {{RELEASE}}/{{NAME}}_signed.wasm registry.gitlab.com/david.haioum/greet-wascc/{{NAME}}-wascc:v{{VERSION}}

login:
	docker login registry.gitlab.com

clean:
	cargo clean
	
keys: account module

account:
	@mkdir -p {{KEYDIR}}
	nk gen account > {{KEYDIR}}/account.txt
	awk '/Seed/{ print $2 }' {{KEYDIR}}/account.txt > {{KEYDIR}}/account.nk

module:
	@mkdir -p {{KEYDIR}}
	nk gen module > {{KEYDIR}}/module.txt
	awk '/Seed/{ print $2 }' {{KEYDIR}}/module.txt > {{KEYDIR}}/module.nk
